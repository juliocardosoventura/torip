var express = require('express');
var router = express.Router();
var CronJob = require('cron').CronJob;
var dns = require('dns');
var ips = Array();
var job = null;
var fs = require('fs');
var logger = null;
router.get('/:dns', function(req, res, next) {
  try{
    logger = fs.createWriteStream("files/"+req.params.dns+'.txt', {flags: 'a'});
    res.render('index', { title: req.params.dns });
    job = new CronJob('* * * * * *', function() {
      dns.resolve4(req.params.dns, function (err, addresses) {
        if (err) throw err;
        addresses.forEach(ip => {
          if(ips.indexOf(ip) < 0){
            console.log(ip);
            ips.push(ip);
            logger.write(ip + '\r\n');
          }
        }); 
      });
    });
    job.start();
  }catch(err){
    console.log(err);
  }
});

router.get('/:dns/stop', function(req, res, next) {
  if(job !== null){
      job.stop();
      if(job !== null)
       logger.end();
      res.render('error', { message: 'Stopped looking at the following dns: ' + req.params.dns, error: { status: ips.join('\r\n')}});
  }else res.render('error', { message: 'You must start running the following dns: ' + req.params.dns, error: { status: '404'}});
});

module.exports = router;
